// Ici mettre le code js pour le menu burger
console.log('Menu Burger');

const navBurger = document.querySelector('div#nav_burger');
const btnBurger = document.querySelector('#link_burger');
const btnClose = document.querySelector('#nav_burger .close');

btnBurger.addEventListener('click', function (e){
    e.preventDefault();
    navBurger.classList.add('open');
})

btnClose.addEventListener('click', function(){
    navBurger.classList.remove('open');
})
