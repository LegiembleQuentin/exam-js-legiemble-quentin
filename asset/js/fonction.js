// Exercices sur la création de fonction
console.log('Fonctions');
// Toutes les réponses vont s'afficher dans la partie gauche du footer

// Selection de boite dans footer
const link_footer_1 = document.querySelector('#link_footer_1');
const link_footer_2 = document.querySelector('#link_footer_2');
const link_footer_3 = document.querySelector('#link_footer_3');
const link_footer_4 = document.querySelector('#link_footer_4');
const link_footer_5 = document.querySelector('#link_footer_5');

/////////////////////
// Exo 1
////////////////////
// Modifier la fonction "showDateInFrench" pour qu'elle renvoie la date du jour sous la forme d'une chaine du type "Le 2 décembre 2022"
// La méthode getMonth(); renvoie un chiffre entier (0 pour janvier, 11 pour décembre)

function showDateInFrench() {
    const months = ['Janvier','Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
    const date = new Date();
    let html = 'Le ' +date.getDate()+ ' ';
    html += months[date.getMonth()]+ ' ';
    html += date.getFullYear();
    return html;
}
link_footer_1.innerText = showDateInFrench();

/////////////////////
// Exo 2
////////////////////
// Modifiez la fonction "isTriangle" pour qu'elle prenne des arguments et renvoie true si c’est un triangle, sinon false.
// Utilisez le théorème d’inégalité. La somme des longueurs de deux côtés d’un triangle est toujours supérieure au troisième côté. Si cela est vrai pour les trois combinaisons, vous avez un triangle.
// isTriangle(34,37,58) => true
// isTriangle(134,77,48) => false
function isTriangle(a, b, c) {
    if ((a + b > c) && (a + c > b) && (b + c > a)){
        return true;
    }else{
        return false;
    }
}
link_footer_2.innerText = isTriangle(34, 37, 58);
/////////////////////
// Exo 3
////////////////////
// Modifiez la fonction "generateStringUser" qui prend en paramètres l'objet "user" et qui renvoie une chaine du type "Michel a 45 ans. Il habite à Pont-Audemer."
const user = {prenom: "Michel", age: 45, ville: "Pont-Audemer"};
function generateStringUser(data) {
    let html = data.prenom+ ' ';
    html += 'a ' +data.age + ' ans. ';
    html += 'Il habite à ' +data.ville+ '.';
    return html;
}
link_footer_3.innerText = generateStringUser(user);
//////////////
// Exo 4
//////////////
// Vous devez modifier la fonction "getAJoke" pour aller chercher une blague au hasard grâce à cette API et à fetch().
// https://joke.deno.dev

// Vous devez retourner de cette fonction une blague, afin de faire afficher la question dans un li et la punchline dans un autre.

// async function getAJoke() {
//     let response = await fetch('https://joke.deno.dev');
//     let data = await response.json();
//     console.log(data)
//     const rdmJoke = data['setup'];
//     return rdmJoke;
// }
//
//
// const joke = getAJoke();
// console.log(joke)
//
// link_footer_4.innerText = joke;
// link_footer_5.innerText = 'punchline';