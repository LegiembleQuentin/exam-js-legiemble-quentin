// Ici mettre le code js pour la palette
console.log('Palette');
const palette = document.querySelector('#palette');
const btnPalette = document.querySelector('#link_palette');
const inputColor = document.querySelector('input[type=color]');
const body = document.querySelector('body');

btnPalette.addEventListener('click', function (e){
    e.preventDefault();
    if(palette.classList.contains('open')){
        palette.classList.remove('open');
    }else{
        palette.classList.add('open');
    }
})

inputColor.addEventListener('change', function (){
    console.log(inputColor.value);
    body.style.backgroundColor = inputColor.value;
    palette.classList.remove('open');
})




