// Ici mettre le code js pour le diaporama
console.log('Diaporama');

type = "text/javascript"
charSet = "utf-8"

$(window).on('load', function () {
    $('#diapo').flexslider({
        animation: "slide",
        directionNav: false,
        controlNav: false,
        slideshowSpeed: 6000,
        animationSpeed: 3000,
    });
});