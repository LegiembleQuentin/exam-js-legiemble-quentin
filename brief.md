## brief examen JS

> Vous venez d'arriver dans une entreprise et votre patron vous demande de continuer un projet déjà initié par un autre développeur. Il a réalisé la partie HTML et une partie du CSS
+ Il reste à écrire le JS et le CSS pour terminer cette page. 
+ La seule partie oû vous devriez intervenir sur le html est la partie diaporama pour structurer le diaporama (#diapo) afin de le rendre fonctionnel, mais si vous préférez intervenir sur le reste du HTML, c'est possible l'important est le résultat final.
+ Vous devez écrire votre css dans base.css, il est interdit de modifier style.css. Les fonctionnalités JS devront être codé dans leurs fichiers dédiés.
+ Vous pouvez utiliser toutes les librairies JS.
+ Renommer le dossier "jsbriefexamen" => "nom-prenom-js"
+ Vous avez 4 heures et vous devez m'envoyer votre devoir soit au format .zip via wetransfert ou alors en créant un repository gitlab public avec votre code avant 16h.
+ Mon e-mail: quidelantoine@gmail.com
### Intégration responsive footer CSS
+ Terminer le footer
+ Ecrire le CSS pour reproduire toutes les parties
+ Voir le dossier "design"
### Burger Menu (burger.js) JS-CSS
+ Lorsque l'écran est plus petit que 950px, au click sur le bouton burger, vous devez faire apparaître venant de la droite le menu (#nav_burger). 
+ Ce menu doit prendre la totalité de l'écran en largeur. 
+ Le click sur la croix ferme le menu.
### Palette de couleur et body (palette.js) JS-CSS
+ Lorsque l'écran est plus petit que 950px, au click sur le bouton palette, vous devez faire apparaître venant de la gauche le formulaire avec le champ de type color (#palette). 
+ Si vous re clicker sur le bouton palette vous faites disparaitre vers la gauche #palette
+ Lorsque l'utilisateur va choisir une couleur, vous devez récupérer son choix et appliquer cette couleur au body et faire disparaitre la boite palette vers la gauche.
### Mise en place diaporama Image et texte (diapo.js) JS-CSS
+ Vous devez mettre en place un diaporama avec chaque slide qui contient une image et un texte (cf design)
+ Le diaporama ne doit pas avoir de border, aucun élément de navigation ne doit être present.
+ Chaque slide doit être visible 6 secondes et le passage d'une slide à une autre doit être lent soit 3 secondes.
+ Les slides doivent défiler de droite vers la gauche et repartir automatiquement à la première slide après avoir vu la dernière.
### Sélecteur d'ingrédients (dropdown.js) JS-CSS
+ Dans le fichier dropdown.js il y a un tableau "ingredients", Vous devez coder un menu déroulant qui liste tous les titres. 
+ Après ouverture du menu, au click sur un des ingrédients de la liste, vous devez afficher en dessous, dans la div#reponse, le détail de l'ingrédient sélectionné, fermer le menu déroulant et mettre le titre et l'icône dans l'entête.
+ Par default, le premier ingrédient est sélectionné.

### Création de fonctions (fonction.js) JS
> Voir asset/js/fonctions.js